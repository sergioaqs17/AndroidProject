package com.moviles.sergioquiroz.tratohechov2.viewControllers.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.moviles.sergioquiroz.tratohechov2.R;
import com.moviles.sergioquiroz.tratohechov2.models.Customers;
import com.moviles.sergioquiroz.tratohechov2.network.ServicesApi;

import org.json.JSONObject;

public class ProfileEditActivity extends AppCompatActivity {
    ViewHolder holder;
    Bundle bundle;
    Customers customer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        bundle = this.getIntent().getExtras();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        customer = Customers.Builder.from(bundle).build();
        holder = new ViewHolder(customer);
    }

    private class ViewHolder{
        TextInputLayout textInputUsername;
        TextInputLayout textInputPassword;
        TextInputLayout textInputConfirmPassword;
        TextInputLayout textInputFirstName;
        TextInputLayout textInputLastName;
        TextInputLayout textInputEmail;
        TextInputLayout textInputDNI;
        TextInputLayout textInputPhone;
        TextInputLayout textInputAddress;
        TextInputLayout textInputReference;
        FloatingActionButton fabSave;

        private static final String TAG = "TratoHecho";

        public ViewHolder(final Customers customer){
            fabSave = (FloatingActionButton) findViewById(R.id.fab_save_user);
            textInputUsername = (TextInputLayout) findViewById(R.id.input_username);
            textInputPassword = (TextInputLayout) findViewById(R.id.input_password);
            textInputConfirmPassword = (TextInputLayout) findViewById(R.id.input_confirmPassword);
            textInputFirstName = (TextInputLayout) findViewById(R.id.input_firstName);
            textInputLastName = (TextInputLayout) findViewById(R.id.input_lastName);
            textInputEmail = (TextInputLayout) findViewById(R.id.input_email);
            textInputDNI = (TextInputLayout) findViewById(R.id.input_DNI);
            textInputPhone = (TextInputLayout) findViewById(R.id.input_phone);
            textInputAddress = (TextInputLayout) findViewById(R.id.input_address);
            textInputReference = (TextInputLayout) findViewById(R.id.input_reference);
            setText(customer);
            fabSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!textInputConfirmPassword.getEditText().getText().toString()
                            .equals(textInputPassword.getEditText().getText().toString()) ||
                            textInputPassword.getEditText().getText().toString().isEmpty() ||
                            textInputUsername.getEditText().getText().toString().isEmpty())
                        Toast.makeText(getApplicationContext(), "Errores al editar", Toast.LENGTH_SHORT).show();

                    else {
                        editCustomer(textInputUsername.getEditText().getText().toString(),
                                textInputPassword.getEditText().getText().toString(),
                                textInputFirstName.getEditText().getText().toString(),
                                textInputLastName.getEditText().getText().toString(),
                                textInputEmail.getEditText().getText().toString(),
                                textInputDNI.getEditText().getText().toString(),
                                textInputPhone.getEditText().getText().toString(),
                                textInputAddress.getEditText().getText().toString(),
                                textInputReference.getEditText().getText().toString(),
                                v, customer);
                    }
                }
            });
        }

        private void editCustomer(String user, String pass, String name, String lastName,
                                 String email, String dni, String phone, String address,
                                 String reference, final View view, Customers customer){
            AndroidNetworking.put(ServicesApi.getCustomerById(String.valueOf(customer.getCustomerId())))
                    .addBodyParameter("login", user)
                    .addBodyParameter("password", pass)
                    .addBodyParameter("name", name)
                    .addBodyParameter("lastName", lastName)
                    .addBodyParameter("email", email)
                    .addBodyParameter("documentTypeId", "1")
                    .addBodyParameter("documentNumber", dni)
                    .addBodyParameter("phoneNumber", phone)
                    .addBodyParameter("address", address)
                    .addBodyParameter("reference", reference)
                    .addBodyParameter("latitude", "1")
                    .addBodyParameter("longitude", "1")
                    .addBodyParameter("rate", "1")
                    .addBodyParameter("online", "true")
                    .addBodyParameter("state", "true")
                    .setTag(TAG)
                    .setPriority(Priority.LOW)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try{
                                finish();
                                Toast.makeText(getApplicationContext(),
                                        "Se edito correctamente", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                Log.d(TAG, String.format("Response Exception: %s", e.getMessage()));
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d(TAG, String.format("onError: %s", anError.getErrorDetail()));
                            finish();
                            Toast.makeText(getApplicationContext(),
                                    "Se edito correctamente", Toast.LENGTH_SHORT).show();
                        }
                    });
        }

        private void setText(Customers customer){
            textInputUsername.getEditText().setText(customer.getLogin());
            textInputFirstName.getEditText().setText(customer.getNames());
            textInputLastName.getEditText().setText(customer.getLastNames());
            textInputEmail.getEditText().setText(customer.getEmail());
            textInputDNI.getEditText().setText(customer.getDocType().getDescription());
            textInputPhone.getEditText().setText(customer.getPhone());
            textInputAddress.getEditText().setText(customer.getAddress());
            textInputReference.getEditText().setText(customer.getReference());
        }
    }



}
