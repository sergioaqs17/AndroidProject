package com.moviles.sergioquiroz.tratohechov2.viewControllers.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.moviles.sergioquiroz.tratohechov2.R;
import com.moviles.sergioquiroz.tratohechov2.models.Customers;
import com.moviles.sergioquiroz.tratohechov2.network.ServicesApi;

import org.json.JSONObject;

import java.util.List;

public class SingUpActivity extends AppCompatActivity {
    ViewHolder holder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_up);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        holder = new ViewHolder();
    }

    private class ViewHolder{
        TextInputLayout textInputUsername;
        TextInputLayout textInputPassword;
        TextInputLayout textInputConfirmPassword;
        TextInputLayout textInputFirstName;
        TextInputLayout textInputLastName;
        TextInputLayout textInputEmail;
        TextInputLayout textInputDNI;
        TextInputLayout textInputPhone;
        TextInputLayout textInputAddress;
        TextInputLayout textInputReference;
        Button btnSingUp;
        CheckBox checkBoxConditions;
        String name;
        private static final String TAG = "TratoHecho";

        public ViewHolder(){
            textInputUsername = (TextInputLayout) findViewById(R.id.input_username);
            textInputPassword = (TextInputLayout) findViewById(R.id.input_password);
            textInputConfirmPassword = (TextInputLayout) findViewById(R.id.input_confirmPassword);
            textInputFirstName = (TextInputLayout) findViewById(R.id.input_firstName);
            textInputLastName = (TextInputLayout) findViewById(R.id.input_lastName);
            textInputEmail = (TextInputLayout) findViewById(R.id.input_email);
            textInputDNI = (TextInputLayout) findViewById(R.id.input_DNI);
            textInputPhone = (TextInputLayout) findViewById(R.id.input_phone);
            textInputAddress = (TextInputLayout) findViewById(R.id.input_address);
            textInputReference = (TextInputLayout) findViewById(R.id.input_reference);
            btnSingUp = (Button) findViewById(R.id.button_singUp_confirm);
            checkBoxConditions = (CheckBox) findViewById(R.id.checkbox_conditions);
            btnSingUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!textInputConfirmPassword.getEditText().getText().toString()
                            .equals(textInputPassword.getEditText().getText().toString()) ||
                            textInputPassword.getEditText().getText().toString().isEmpty() ||
                            textInputUsername.getEditText().getText().toString().isEmpty() ||
                            !checkBoxConditions.isChecked())
                        Toast.makeText(getApplicationContext(), "Errores en el registro", Toast.LENGTH_SHORT).show();

                    else {
                        addCustomer(textInputUsername.getEditText().getText().toString(),
                                textInputPassword.getEditText().getText().toString(),
                                textInputFirstName.getEditText().getText().toString(),
                                textInputLastName.getEditText().getText().toString(),
                                textInputEmail.getEditText().getText().toString(),
                                textInputPhone.getEditText().getText().toString(),
                                textInputAddress.getEditText().getText().toString(),
                                textInputReference.getEditText().getText().toString(),
                                v);
                    }
                }
            });

        }

        private void addCustomer(String user, String pass, String name, String lastName,
                                 String email, String phone, String address,
                                 String reference, final View view){
            AndroidNetworking.post(ServicesApi.getClientsUrl())
                    .addBodyParameter("login", user)
                    .addBodyParameter("password", pass)
                    .addBodyParameter("name", name)
                    .addBodyParameter("lastName", lastName)
                    .addBodyParameter("email", email)
                    .addBodyParameter("document", "1")
                    .addBodyParameter("phoneNumber", phone)
                    .addBodyParameter("address", address)
                    .addBodyParameter("reference", reference)
                    .addBodyParameter("latitude", "1")
                    .addBodyParameter("longitude", "1")
                    .addBodyParameter("rate", "2")
                    .addBodyParameter("online", "true")
                    .addBodyParameter("state", "true")
                    .setTag(TAG)
                    .setPriority(Priority.LOW)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try{
                                Customers customer = Customers.Builder.from(response).build();
                                navigateToMain(customer, view);
                            } catch (Exception e) {
                                Log.d(TAG, String.format("Response Exception: %s", e.getMessage()));
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d(TAG, String.format("onError: %s", anError.getErrorDetail()));
                        }
                    });
        }

        private void navigateToMain(Customers customer, View v) {
            Context context = v.getContext();
            name = customer.getNames();
            Toast.makeText(getApplicationContext(), "Welcome "
                    + name, Toast.LENGTH_SHORT).show();
            context.startActivity(new Intent(context, MainCustomerActivity.class).putExtras(customer.toBundle()));
        }
    }

}
