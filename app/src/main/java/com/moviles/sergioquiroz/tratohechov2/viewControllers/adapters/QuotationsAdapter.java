package com.moviles.sergioquiroz.tratohechov2.viewControllers.adapters;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.moviles.sergioquiroz.tratohechov2.R;
import com.moviles.sergioquiroz.tratohechov2.models.Quotations;

import java.util.List;

public class QuotationsAdapter extends RecyclerView.Adapter<QuotationsAdapter.ViewHolder> {
    private List<Quotations> quotations;
    private Quotations quotation;

    public QuotationsAdapter() {
    }

    public QuotationsAdapter(List<Quotations> quotations) {
        this.quotations = quotations;
    }

    @NonNull
    @Override
    public QuotationsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_quotation,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull QuotationsAdapter.ViewHolder holder, int position) {
        holder.updateViews(quotations.get(position));
    }

    @Override
    public int getItemCount() {
        return quotations.size();
    }

    public List<Quotations> getQuotations() {
        return quotations;
    }

    public QuotationsAdapter setQuotations(List<Quotations> quotations) {
        this.quotations = quotations;
        return this;
    }

    public Quotations getQuotation() {
        return quotation;
    }

    public QuotationsAdapter setQuotation(Quotations quotation) {
        this.quotation = quotation;
        return this;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textNameSpecialist;
        private TextView textQuotationsMaterials;
        private TextView textQuotationsPrice;
        private TextView textQuotationsTime;
        private TextView getTextQuotationsDescription;
        private RatingBar ratingBar;
        private ConstraintLayout itemLayout;
        private final String ACT = "Si";
        private final String PAS = "No";
        private String name;

        public ViewHolder(View itemView) {
            super(itemView);
            textNameSpecialist = (TextView) itemView.findViewById(R.id.quotations_specialist_name);
            textQuotationsMaterials = (TextView) itemView.findViewById(R.id.quotations_materials);
            textQuotationsPrice = (TextView) itemView.findViewById(R.id.quotations_price);
            textQuotationsTime = (TextView) itemView.findViewById(R.id.quotations_time);
            getTextQuotationsDescription = (TextView) itemView.findViewById(R.id.quotations_description);
            ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar_quotation_specialist);
            itemLayout = (ConstraintLayout) itemView.findViewById(R.id.layout_item);
        }

        public void updateViews(final Quotations quotations){
            name =  quotations.getSpecialist().getNames() + quotations.getSpecialist().getLastNames();
            textNameSpecialist.setText(name);
            if (quotations.isIncludesMaterial())
                textQuotationsMaterials.setText(ACT);
            else
                textQuotationsMaterials.setText(PAS);
            textQuotationsPrice.setText(String.valueOf(quotations.getPrice()));
            textQuotationsTime.setText(String.valueOf(quotations.getEstimatedTime()));
            getTextQuotationsDescription.setText(quotations.getDescription());
            ratingBar.setRating((float)quotations.getSpecialist().getRate());
            itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }
}
