package com.moviles.sergioquiroz.tratohechov2.viewControllers.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.moviles.sergioquiroz.tratohechov2.R;

public class SplashActivity extends AppCompatActivity {

    private final int splashDisplayLength = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this,OnBoardingActivity.class);
                SplashActivity.this.startActivity(intent);
                SplashActivity.this.finish();
            }
        }, splashDisplayLength);

    }




}
