package com.moviles.sergioquiroz.tratohechov2.viewControllers.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.arlib.floatingsearchview.FloatingSearchView;
import com.moviles.sergioquiroz.tratohechov2.R;
import com.moviles.sergioquiroz.tratohechov2.models.Specialists;
import com.moviles.sergioquiroz.tratohechov2.network.ServicesApi;
import com.moviles.sergioquiroz.tratohechov2.viewControllers.adapters.SpecialistsAdapter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {
    FloatingSearchView searchView;
    SpecialistsAdapter adapter;
    RecyclerView specialistsRecyclerView;
    LinearLayoutManager layoutManager;
    List<Specialists> specialists;
    private static final String TAG = "TratoHecho";


    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        specialistsRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_specialists);
        searchView = (FloatingSearchView) view.findViewById(R.id.floating_search_view);
        specialists = new ArrayList<>();
        adapter = new SpecialistsAdapter(specialists);
        layoutManager = new LinearLayoutManager(view.getContext());
        specialistsRecyclerView.setAdapter(adapter);
        specialistsRecyclerView.setLayoutManager(layoutManager);
        updateData();
        return view;
    }

    private void updateData(){
        AndroidNetworking.get(ServicesApi.getSpecialistsUrl())
                .setTag(TAG)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            specialists = Specialists.Builder.from(
                                    response.optJSONArray("specialists")).buildAll();
                            adapter.setSpecialists(specialists);
                            adapter.notifyDataSetChanged();
                        } catch (Exception e){
                            Log.d(TAG, String.format("Response Exception: %s", e.getMessage()));
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, String.format("onError: %s", anError.getErrorDetail()));
                    }
                });
    }
}
