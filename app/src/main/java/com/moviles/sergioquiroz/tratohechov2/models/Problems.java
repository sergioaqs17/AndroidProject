package com.moviles.sergioquiroz.tratohechov2.models;

import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Problems {
    private int problemsId;
    private Customers customer;
    private String title;
    private String description;
    private int state;

    public Problems() {
    }

    public Problems(int problemsId, Customers customer, String title, String description, int state) {
        this.problemsId = problemsId;
        this.customer = customer;
        this.title = title;
        this.description = description;
        this.state = state;
    }

    public int getProblemsId() {
        return problemsId;
    }

    public Problems setProblemsId(int problemsId) {
        this.problemsId = problemsId;
        return this;
    }

    public Customers getCustomer() {
        return customer;
    }

    public Problems setCustomer(Customers customer) {
        this.customer = customer;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Problems setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Problems setDescription(String description) {
        this.description = description;
        return this;
    }

    public int isState() {
        return state;
    }

    public Problems setState(int state) {
        this.state = state;
        return this;
    }

    public Bundle toBundle(){
        Bundle bundle = new Bundle();
        bundle.putInt("id", getProblemsId());
        bundle.putBundle("customer", getCustomer().toBundle());
        bundle.putString("title", getTitle());
        bundle.putString("description", getDescription());
        bundle.putInt("state", isState());
        return bundle;
    }

    public static class Builder {
        Problems problem;
        List<Problems> problems;

        public Builder() {
            problem = new Problems();
            problems = new ArrayList<>();
        }

        public Builder(Problems problem) {
            this.problem = problem;
        }

        public Builder(List<Problems> problems) {
            this.problems = problems;
        }

        public Problems build() {
            return problem;
        }

        public List<Problems> buildAll() {
            return problems;
        }

        public static Problems.Builder from(JSONObject jsonProblem) {
            try {
                return new Problems.Builder(new Problems(
                        jsonProblem.getInt("id"),
                        Customers.Builder.from(jsonProblem.getJSONObject("customer")).build(),
                        jsonProblem.getString("title"),
                        jsonProblem.getString("description"),
                        jsonProblem.getInt("state")
                ));
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }

        public static Problems.Builder from(JSONArray jsonProblems) {
            int length = jsonProblems.length();
            List<Problems> problems = new ArrayList<>();
            for (int i = 0; i < length; i++) {
                try {
                    problems.add(Problems.Builder.from(jsonProblems.getJSONObject(i)).build());
                } catch (JSONException | NullPointerException e) {
                    e.printStackTrace();
                }
            }
            return new Problems.Builder(problems);
        }

        public static Problems.Builder from(Bundle bundle) {
            return new Problems.Builder(new Problems(
                    bundle.getInt("id"),
                    Customers.Builder.from(bundle.getBundle("customer")).build(),
                    bundle.getString("title"),
                    bundle.getString("description"),
                    bundle.getInt("state")
            ));
        }
    }


}
