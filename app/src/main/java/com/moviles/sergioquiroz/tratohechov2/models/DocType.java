package com.moviles.sergioquiroz.tratohechov2.models;

import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DocType {
    private int docTypeId;
    private String description;

    public DocType() {
    }

    public DocType(int docTypeId, String description) {
        this.docTypeId = docTypeId;
        this.description = description;
    }

    public DocType(int docTypeId) {
        this.docTypeId = docTypeId;
    }

    public int getDocTypeId() {
        return docTypeId;
    }

    public DocType setDocTypeId(int docTypeId) {
        this.docTypeId = docTypeId;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public DocType setDescription(String description) {
        this.description = description;
        return this;
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt("id", getDocTypeId());
        bundle.putString("description", getDescription());
        return bundle;
    }

    public static class Builder{
        DocType docType;
        List<DocType> docTypeList;

        public Builder() {
            docType = new DocType();
            docTypeList = new ArrayList<>();
        }

        public Builder(DocType docType) {
            this.docType = docType;
        }

        public Builder(List<DocType> docTypeList) {
            this.docTypeList = docTypeList;
        }

        public DocType build() {
            return docType;
        }

        public List<DocType> buildAll() {
            return docTypeList;
        }

        public static Builder from(JSONObject jsonDocType) {
            try {
                return new Builder(new DocType(
                        jsonDocType.getInt("id"),
                        jsonDocType.getString("description")
                ));
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }

        public static Builder from(JSONArray jsonDocTypeList) {
            int length = jsonDocTypeList.length();
            List<DocType> docTypeList = new ArrayList<>();
            for (int i = 0; i < length; i++) {
                try {
                    docTypeList.add(Builder.from(jsonDocTypeList.getJSONObject(i)).build());
                } catch (JSONException | NullPointerException e) {
                    e.printStackTrace();
                }
            }
            return new Builder(docTypeList);
        }

        public static Builder from(Bundle bundle) {
            return new Builder(new DocType(
                    bundle.getInt("id"),
                    bundle.getString("description")
            ));
        }
    }
}
