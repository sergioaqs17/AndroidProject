package com.moviles.sergioquiroz.tratohechov2.models;

import android.os.Bundle;

public class Customer {
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private String DNI;

    public Customer() {
    }

    public Customer(String username, String password, String firstName, String lastName, String email, String DNI) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.DNI = DNI;
    }

    public String getUsername() {
        return username;
    }

    public Customer setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Customer setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Customer setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Customer setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Customer setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getDNI() {
        return DNI;
    }

    public Customer setDNI(String DNI) {
        this.DNI = DNI;
        return this;
    }

    public Bundle toBundle(){
        Bundle bundle = new Bundle();
        bundle.putString("username", getUsername());
        bundle.putString("password", getPassword());
        bundle.putString("firstName", getUsername());
        bundle.putString("lastName", getLastName());
        bundle.putString("email", getEmail());
        bundle.putString("DNI", getDNI());
        return bundle;
    }

    public static Customer fromBundle(Bundle bundle) {
        return new Customer(
                bundle.getString("username"),
                bundle.getString("password"),
                bundle.getString("firstName"),
                bundle.getString("lastName"),
                bundle.getString("email"),
                bundle.getString("DNI")
        );
    }
}
