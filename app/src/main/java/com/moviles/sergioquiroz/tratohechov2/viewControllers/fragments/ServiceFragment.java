package com.moviles.sergioquiroz.tratohechov2.viewControllers.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.moviles.sergioquiroz.tratohechov2.models.Customers;
import com.moviles.sergioquiroz.tratohechov2.models.Problems;
import com.moviles.sergioquiroz.tratohechov2.R;
import com.moviles.sergioquiroz.tratohechov2.network.ServicesApi;
import com.moviles.sergioquiroz.tratohechov2.viewControllers.activities.AddProblemActivity;
import com.moviles.sergioquiroz.tratohechov2.viewControllers.adapters.ProblemsAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceFragment extends Fragment {
    RecyclerView problemsRecyclerView;
    ProblemsAdapter adapter;
    LinearLayoutManager layoutManager;
    FloatingActionButton fabAddProblem;
    List<Problems> problems;
    Customers customers;
    private static final String TAG = "TratoHecho";


    public ServiceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume(){
        super.onResume();
            adapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_service, container, false);
        problemsRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_services);
        fabAddProblem = (FloatingActionButton) view.findViewById(R.id.fab_addProblem);
        problems = new ArrayList<>();
        adapter = new ProblemsAdapter(problems);
        layoutManager = new LinearLayoutManager(view.getContext());
        problemsRecyclerView.setAdapter(adapter);
        problemsRecyclerView.setLayoutManager(layoutManager);
        fabAddProblem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), AddProblemActivity.class));
            }
        });
        Bundle bundle = this.getArguments();
        assert bundle != null;
        customers = Customers.Builder.from(bundle).build();
        updateData(customers);
        return view;
    }

    private void updateData(Customers customer){
        AndroidNetworking.get(ServicesApi.getCustomerProblems(String.valueOf(customer.getCustomerId())))
                .setTag(TAG)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            problems = Problems.Builder.from(
                                    response.optJSONArray("problems")).buildAll();
                            adapter.setProblems(problems);
                            adapter.notifyDataSetChanged();
                        } catch (Exception e){
                            Log.d(TAG, String.format("Response Exception: %s", e.getMessage()));
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, String.format("onError: %s", anError.getErrorDetail()));
                    }
                });
    }
}
