package com.moviles.sergioquiroz.tratohechov2.models;

import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Favorite {
    private int favoriteId;
    private Customers customerId;
    private Specialists specialistId;
    private boolean hidden;

    public Favorite() {
    }

    public Favorite(int favoriteId, Customers customerId, Specialists specialistId, boolean hidden) {
        this.favoriteId = favoriteId;
        this.customerId = customerId;
        this.specialistId = specialistId;
        this.hidden = hidden;
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt("id", getFavoriteId());
        bundle.putBundle("customer", getCustomerId().toBundle());
        bundle.putBundle("specialist", getSpecialistId().toBundle());
        bundle.putBoolean("hidden", isHidden());
        return bundle;
    }

    public int getFavoriteId() {
        return favoriteId;
    }

    public Favorite setFavoriteId(int favoriteId) {
        this.favoriteId = favoriteId;
        return this;
    }

    public Customers getCustomerId() {
        return customerId;
    }

    public Favorite setCustomerId(Customers customerId) {
        this.customerId = customerId;
        return this;
    }

    public Specialists getSpecialistId() {
        return specialistId;
    }

    public Favorite setSpecialistId(Specialists specialistId) {
        this.specialistId = specialistId;
        return this;
    }

    public boolean isHidden() {
        return hidden;
    }

    public Favorite setHidden(boolean hidden) {
        this.hidden = hidden;
        return this;
    }

    public static class Builder {
        Favorite favorite;
        List<Favorite> favoriteList;

        public Builder() {
            favorite = new Favorite();
            favoriteList = new ArrayList<>();
        }

        public Builder(Favorite favorite) {
            this.favorite = favorite;
        }

        public Builder(List<Favorite> favoriteList) {
            this.favoriteList = favoriteList;
        }

        public Favorite build() {
            return favorite;
        }

        public List<Favorite> buildAll() {
            return favoriteList;
        }

        public static Builder from(JSONObject jsonFavorite) {
            try {
                return new Builder(new Favorite(
                        jsonFavorite.getInt("id"),
                        Customers.Builder.from(jsonFavorite.getJSONObject("customer")).build(),
                        Specialists.Builder.from(jsonFavorite.getJSONObject("specialist")).build(),
                        jsonFavorite.getBoolean("hidden")
                ));
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }

        public static Builder from(JSONArray jsonFavoriteList) {
            int length = jsonFavoriteList.length();
            List<Favorite> favoriteList = new ArrayList<>();
            for (int i = 0; i < length; i++) {
                try {
                    favoriteList.add(Builder.from(jsonFavoriteList.getJSONObject(i)).build());
                } catch (JSONException | NullPointerException e) {
                    e.printStackTrace();
                }
            }
            return new Builder(favoriteList);
        }

        public static Builder from(Bundle bundle) {
            return new Builder(new Favorite(
                    bundle.getInt("id"),
                    Customers.Builder.from(bundle.getBundle("customer")).build(),
                    Specialists.Builder.from(bundle.getBundle("specialist")).build(),
                    bundle.getBoolean("hidden")
            ));
        }
    }
}
