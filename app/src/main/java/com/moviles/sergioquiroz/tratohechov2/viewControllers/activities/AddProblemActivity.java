package com.moviles.sergioquiroz.tratohechov2.viewControllers.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.moviles.sergioquiroz.tratohechov2.R;
import com.moviles.sergioquiroz.tratohechov2.network.ServicesApi;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class AddProblemActivity extends AppCompatActivity {
    ViewHolder holder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_problem);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        holder = new ViewHolder();
    }



    public class ViewHolder{
        SharedPreferences sharedPreferences;
        TextInputLayout textInputTitle;
        TextInputLayout textInputDescription;
        Button btnAddProblem;
        int key;
        private static final String TAG = "TratoHecho";

        public ViewHolder(){
            sharedPreferences = getSharedPreferences("TratoHecho", Context.MODE_PRIVATE);
            textInputTitle = (TextInputLayout) findViewById(R.id.input_problemTitle);
            textInputDescription = (TextInputLayout) findViewById(R.id.input_problemDescription);
            btnAddProblem = (Button) findViewById(R.id.button_addProblem);
            if (sharedPreferences.contains("customerKey"))
                key = sharedPreferences.getInt("customerKey",-1);
            btnAddProblem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (textInputTitle.getEditText().getText().toString().isEmpty() ||
                            textInputDescription.getEditText().getText().toString().isEmpty())
                        Toast.makeText(getApplicationContext(), "Campos Obligatorios",
                                Toast.LENGTH_SHORT).show();

                    else {
                        addProblem(textInputTitle.getEditText().getText().toString(),
                                textInputDescription.getEditText().getText().toString(), key);
                    }
                }
            });
        }

        private void addProblem(String title, String description, int key){
            AndroidNetworking.post(ServicesApi.getCustomerProblems(String.valueOf(key)))
                    .addBodyParameter("title", title)
                    .addBodyParameter("description", description)
                    .addBodyParameter("state", "0")
                    .setTag(TAG)
                    .setPriority(Priority.LOW)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try{
                                navigateToMain();
                            } catch (Exception e) {
                                Log.d(TAG, String.format("Response Exception: %s", e.getMessage()));
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d(TAG, String.format("onError: %s", anError.getErrorDetail()));
                        }
                    });
        }

        private void navigateToMain() {
            Toast.makeText(getApplicationContext(), "Problema creado"
                    , Toast.LENGTH_SHORT).show();
            finish();
        }
    }

}
