package com.moviles.sergioquiroz.tratohechov2.viewControllers.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.moviles.sergioquiroz.tratohechov2.R;
import com.moviles.sergioquiroz.tratohechov2.viewControllers.fragments.FavoritesFragment;
import com.moviles.sergioquiroz.tratohechov2.viewControllers.fragments.ProfileFragment;
import com.moviles.sergioquiroz.tratohechov2.viewControllers.fragments.SearchFragment;
import com.moviles.sergioquiroz.tratohechov2.viewControllers.fragments.ServiceFragment;

public class MainCustomerActivity extends AppCompatActivity {
    Bundle bundle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        bundle = this.getIntent().getExtras();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_customer);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(monNavigationItemSelectedListener);
        int itemId =  navigation.getSelectedItemId() == 0 ? R.id.navigation_home : navigation.getSelectedItemId();
        navigateTo(navigation.getMenu().findItem(itemId));
    }

    private BottomNavigationView.OnNavigationItemSelectedListener
            monNavigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            navigateTo(item);
            return true;
        }
    };

    private void navigateTo(MenuItem item) {
        item.setChecked(true);
        Fragment fragment = getFragmentFor(item);
        //Log.d("Nombre del sujeto", bundle.getString("name"));
        fragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content, fragment)
                .commit();
    }

    private Fragment getFragmentFor(MenuItem item) {
        switch (item.getItemId()){
            case R.id.navigation_home: return new SearchFragment();
            case R.id.navigation_favorites: return new FavoritesFragment();
            case R.id.navigation_services: return new ServiceFragment();
            case R.id.navigation_profile: return new ProfileFragment();
            default: return new SearchFragment();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_on_boarding, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
            return true;
        }
        if (id == R.id.action_singOut) {
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            return true;
        }
        if (id == R.id.action_help) {
            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
