package com.moviles.sergioquiroz.tratohechov2.viewControllers.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moviles.sergioquiroz.tratohechov2.R;
import com.moviles.sergioquiroz.tratohechov2.models.Problems;
import com.moviles.sergioquiroz.tratohechov2.viewControllers.activities.QuotationsActivity;

import java.util.List;

public class ProblemsAdapter extends RecyclerView.Adapter<ProblemsAdapter.ViewHolder> {
    private List<Problems> problems;
    private Problems problem;


    public ProblemsAdapter() {
    }

    public ProblemsAdapter(List<Problems> problems) {
        this.problems = problems;
    }

    @NonNull
    @Override
    public ProblemsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_service,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProblemsAdapter.ViewHolder holder, int position) {
        holder.updateViews(problems.get(position));
    }

    @Override
    public int getItemCount() {
        return problems.size();
    }

    public List<Problems> getProblems() {
        return problems;
    }

    public ProblemsAdapter setProblems(List<Problems> problems) {
        this.problems = problems;
        return this;
    }

    public Problems getProblem() {
        return problem;
    }

    public ProblemsAdapter setProblem(Problems problem) {
        this.problem = problem;
        return this;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView titleTextView;
        private TextView tagTextView;
        private TextView stateTextView;
        private TextView descriptionTextView;
        private ConstraintLayout itemLayout;
        private final String ACT = "Pendiente";
        private final String PAS = "En Proceso";

        public ViewHolder(View itemView) {
            super(itemView);
            titleTextView = (TextView) itemView.findViewById(R.id.title_service);
            tagTextView = (TextView) itemView.findViewById(R.id.tag_service);
            stateTextView = (TextView) itemView.findViewById(R.id.state_service);
            descriptionTextView = (TextView) itemView.findViewById(R.id.description_service);
            itemLayout = (ConstraintLayout) itemView.findViewById(R.id.layout_item);
        }

        public void updateViews(final Problems fromProblem){
            titleTextView.setText(fromProblem.getTitle());
            if (fromProblem.isState() == 0)
                stateTextView.setText(ACT);
            else
                stateTextView.setText(PAS);
            descriptionTextView.setText(fromProblem.getDescription());
            itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    context.startActivity(new Intent(context, QuotationsActivity.class)
                            .putExtras(fromProblem.toBundle()));
                }
            });
        }
    }
}
