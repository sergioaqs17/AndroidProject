package com.moviles.sergioquiroz.tratohechov2.models;

import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Customers {
    private int customerId;
    private String login;
    private String password;
    private String names;
    private String lastNames;
    private DocType docType;
    private String phone;
    private String email;
    private String address;
    private String reference;
    private double rate;
    private boolean state;
    private boolean hidden;
    private double latitude;
    private double longitude;

    public Customers() {
    }

    public Customers(int customerId, String login, String password, String names, String lastNames,
                     DocType docType, String phone, String email, String address,
                     String reference, double rate, boolean state, boolean hidden,
                     double latitude, double longitude) {
        this.customerId = customerId;
        this.login = login;
        this.password = password;
        this.names = names;
        this.lastNames = lastNames;
        this.docType = docType;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.reference = reference;
        this.rate = rate;
        this.state = state;
        this.hidden = hidden;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Customers(int customerId) {
        this.customerId = customerId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public Customers setCustomerId(int customerId) {
        this.customerId = customerId;
        return this;
    }

    public String getLogin() {
        return login;
    }

    public Customers setLogin(String login) {
        this.login = login;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Customers setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getNames() {
        return names;
    }

    public Customers setNames(String names) {
        this.names = names;
        return this;
    }

    public String getLastNames() {
        return lastNames;
    }

    public Customers setLastNames(String lastNames) {
        this.lastNames = lastNames;
        return this;
    }

    public DocType getDocType() {
        return docType;
    }

    public Customers setDocType(DocType docType) {
        this.docType = docType;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public Customers setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Customers setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Customers setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getReference() {
        return reference;
    }

    public Customers setReference(String reference) {
        this.reference = reference;
        return this;
    }

    public double getRate() {
        return rate;
    }

    public Customers setRate(double rate) {
        this.rate = rate;
        return this;
    }

    public boolean isState() {
        return state;
    }

    public Customers setState(boolean state) {
        this.state = state;
        return this;
    }

    public boolean isHidden() {
        return hidden;
    }

    public Customers setHidden(boolean hidden) {
        this.hidden = hidden;
        return this;
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt("id", getCustomerId());
        bundle.putString("login", getLogin());
        bundle.putString("password", getPassword());
        bundle.putString("name", getNames());
        bundle.putString("lastName", getLastNames());
        bundle.putBundle("document", getDocType().toBundle());
        bundle.putString("phoneNumber", getPhone());
        bundle.putString("email", getEmail());
        bundle.putString("address", getAddress());
        bundle.putString("reference", getReference());
        bundle.putDouble("rate", getRate());
        bundle.putBoolean("state", isState());
        bundle.putBoolean("online", isHidden());
        bundle.putDouble("latitude", getLatitude());
        bundle.putDouble("longitude", getLongitude());
        return bundle;
    }

    public double getLatitude() {
        return latitude;
    }

    public Customers setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public double getLongitude() {
        return longitude;
    }

    public Customers setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public static class Builder {
        Customers customer;
        List<Customers> customers;

        public Builder() {
            customer = new Customers();
            customers = new ArrayList<>();
        }

        public Builder(Customers customer) {
            this.customer = customer;
        }

        public Builder(List<Customers> customers) {
            this.customers = customers;
        }

        public Customers build() {
            return customer;
        }

        public List<Customers> buildAll() {
            return customers;
        }

        public static Builder from(JSONObject jsonCustomer) {
            try {
                return new Builder(new Customers(
                        jsonCustomer.getInt("id"),
                        jsonCustomer.getString("login"),
                        jsonCustomer.getString("password"),
                        jsonCustomer.getString("name"),
                        jsonCustomer.getString("lastName"),
                        DocType.Builder.from(jsonCustomer.getJSONObject("document")).build(),
                        jsonCustomer.getString("phoneNumber"),
                        jsonCustomer.getString("email"),
                        jsonCustomer.getString("address"),
                        jsonCustomer.getString("reference"),
                        jsonCustomer.getDouble("rate"),
                        jsonCustomer.getBoolean("state"),
                        jsonCustomer.getBoolean("online"),
                        jsonCustomer.getDouble("latitude"),
                        jsonCustomer.getDouble("longitude")
                ));
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }

        public static Builder from(JSONArray jsonCustomerList) {
            int length = jsonCustomerList.length();
            List<Customers> customerList = new ArrayList<>();
            for (int i = 0; i < length; i++) {
                try {
                    customerList.add(Builder.from(jsonCustomerList.getJSONObject(i)).build());
                } catch (JSONException | NullPointerException e) {
                    e.printStackTrace();
                }
            }
            return new Builder(customerList);
        }

        public static Builder from(Bundle bundle) {
            return new Builder(new Customers(
                    bundle.getInt("id"),
                    bundle.getString("login"),
                    bundle.getString("password"),
                    bundle.getString("name"),
                    bundle.getString("lastName"),
                    DocType.Builder.from(bundle.getBundle("document")).build(),
                    bundle.getString("phoneNumber"),
                    bundle.getString("email"),
                    bundle.getString("address"),
                    bundle.getString("reference"),
                    bundle.getDouble("rate"),
                    bundle.getBoolean("state"),
                    bundle.getBoolean("online"),
                    bundle.getDouble("latitude"),
                    bundle.getDouble("longitude")
            ));
        }
    }

}
