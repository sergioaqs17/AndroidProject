package com.moviles.sergioquiroz.tratohechov2.models;

import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Quotations {
    private int id;
    private String description;
    private Double price;
    private int estimatedTime;
    private boolean includesMaterial;
    private int state;
    private String startDate;
    private String finishDate;
    private Double finalPrice;
    private Problems problem;
    private Specialists specialist;
    private Double specialistRate;
    private String specialistComment;
    private Double customerRate;
    private String customerComment;

    public Quotations() {
    }

    public Quotations(int id, String description, Double price, int estimatedTime,
                      boolean includesMaterial, int state, String startDate, String finishDate,
                      Double finalPrice, Problems problem, Specialists specialist,
                      Double specialistRate, String specialistComment, Double customerRate,
                      String customerComment) {
        this.id = id;
        this.description = description;
        this.price = price;
        this.estimatedTime = estimatedTime;
        this.includesMaterial = includesMaterial;
        this.state = state;
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.finalPrice = finalPrice;
        this.problem = problem;
        this.specialist = specialist;
        this.specialistRate = specialistRate;
        this.specialistComment = specialistComment;
        this.customerRate = customerRate;
        this.customerComment = customerComment;
    }

    public int getId() {
        return id;
    }

    public Quotations setId(int id) {
        this.id = id;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Quotations setDescription(String description) {
        this.description = description;
        return this;
    }

    public Double getPrice() {
        return price;
    }

    public Quotations setPrice(Double price) {
        this.price = price;
        return this;
    }

    public int getEstimatedTime() {
        return estimatedTime;
    }

    public Quotations setEstimatedTime(int estimatedTime) {
        this.estimatedTime = estimatedTime;
        return this;
    }

    public boolean isIncludesMaterial() {
        return includesMaterial;
    }

    public Quotations setIncludesMaterial(boolean includesMaterial) {
        this.includesMaterial = includesMaterial;
        return this;
    }

    public int getState() {
        return state;
    }

    public Quotations setState(int state) {
        this.state = state;
        return this;
    }

    public String getStartDate() {
        return startDate;
    }

    public Quotations setStartDate(String startDate) {
        this.startDate = startDate;
        return this;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public Quotations setFinishDate(String finishDate) {
        this.finishDate = finishDate;
        return this;
    }

    public Double getFinalPrice() {
        return finalPrice;
    }

    public Quotations setFinalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
        return this;
    }

    public Problems getProblem() {
        return problem;
    }

    public Quotations setProblem(Problems problem) {
        this.problem = problem;
        return this;
    }

    public Specialists getSpecialist() {
        return specialist;
    }

    public Quotations setSpecialist(Specialists specialist) {
        this.specialist = specialist;
        return this;
    }

    public Double getSpecialistRate() {
        return specialistRate;
    }

    public Quotations setSpecialistRate(Double specialistRate) {
        this.specialistRate = specialistRate;
        return this;
    }

    public String getSpecialistComment() {
        return specialistComment;
    }

    public Quotations setSpecialistComment(String specialistComment) {
        this.specialistComment = specialistComment;
        return this;
    }

    public Double getCustomerRate() {
        return customerRate;
    }

    public Quotations setCustomerRate(Double customerRate) {
        this.customerRate = customerRate;
        return this;
    }

    public String getCustomerComment() {
        return customerComment;
    }

    public Quotations setCustomerComment(String customerComment) {
        this.customerComment = customerComment;
        return this;
    }

    public Bundle toBundle(){
        Bundle bundle = new Bundle();
        bundle.putInt("id", getId());
        bundle.putString("description", getDescription());
        bundle.putDouble("price", getPrice());
        bundle.putInt("estimatedTime", getEstimatedTime());
        bundle.putBoolean("includesMaterial", isIncludesMaterial());
        bundle.putInt("state", getState());
        bundle.putString("startDate", getStartDate());
        bundle.putString("finishDate", getFinishDate());
        bundle.putDouble("finalPrice", getFinalPrice());
        bundle.putBundle("problem", getProblem().toBundle());
        bundle.putBundle("specialist", getSpecialist().toBundle());
        bundle.putDouble("specialistRate", getSpecialistRate());
        bundle.putString("specialistComment", getSpecialistComment());
        bundle.putDouble("customerRate", getCustomerRate());
        bundle.putString("customerComment", getCustomerComment());
        return bundle;
    }

    public static class Builder {
        Quotations quotations;
        List<Quotations> quotationsList;

        public Builder() {
            quotations = new Quotations();
            quotationsList = new ArrayList<>();
        }

        public Builder(Quotations quotations) {
            this.quotations = quotations;
        }

        public Builder(List<Quotations> quotationsList) {
            this.quotationsList = quotationsList;
        }

        public Quotations build() {
            return quotations;
        }

        public List<Quotations> buildAll() {
            return quotationsList;
        }

        public static Quotations.Builder from(JSONObject jsonQuotations) {
            try {
                return new Quotations.Builder(new Quotations(
                        jsonQuotations.getInt("id"),
                        jsonQuotations.getString("description"),
                        jsonQuotations.getDouble("price"),
                        jsonQuotations.getInt("estimatedTime"),
                        jsonQuotations.getBoolean("includesMaterial"),
                        jsonQuotations.getInt("state"),
                        jsonQuotations.getString("startDate"),
                        jsonQuotations.getString("finishDate"),
                        jsonQuotations.getDouble("finalPrice"),
                        Problems.Builder.from(jsonQuotations.getJSONObject("problem")).build(),
                        Specialists.Builder.from(jsonQuotations.getJSONObject("specialist")).build(),
                        jsonQuotations.getDouble("specialistRate"),
                        jsonQuotations.getString("specialistComment"),
                        jsonQuotations.getDouble("customerRate"),
                        jsonQuotations.getString("customerComment")
                ));
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }

        public static Quotations.Builder from(JSONArray jsonQuotationsList) {
            int length = jsonQuotationsList.length();
            List<Quotations> quotationsList = new ArrayList<>();
            for (int i = 0; i < length; i++) {
                try {
                    quotationsList.add(Quotations.Builder.from(jsonQuotationsList.getJSONObject(i)).build());
                } catch (JSONException | NullPointerException e) {
                    e.printStackTrace();
                }
            }
            return new Quotations.Builder(quotationsList);
        }

        public static Quotations.Builder from(Bundle bundle) {
            return new Quotations.Builder(new Quotations(
                    bundle.getInt("id"),
                    bundle.getString("description"),
                    bundle.getDouble("price"),
                    bundle.getInt("estimatedTime"),
                    bundle.getBoolean("includesMaterial"),
                    bundle.getInt("state"),
                    bundle.getString("startDate"),
                    bundle.getString("finishDate"),
                    bundle.getDouble("finalPrice"),
                    Problems.Builder.from(bundle.getBundle("problem")).build(),
                    Specialists.Builder.from(bundle.getBundle("specialist")).build(),
                    bundle.getDouble("specialistRate"),
                    bundle.getString("specialistComment"),
                    bundle.getDouble("customerRate"),
                    bundle.getString("customerComment")
            ));
        }
    }
}
