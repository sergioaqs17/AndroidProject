package com.moviles.sergioquiroz.tratohechov2.models;

import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Tag {
    private int tagId;
    private String description;

    public Tag() {
    }

    public Tag(int tagId, String description) {
        this.tagId = tagId;
        this.description = description;
    }

    public int getTagId() {
        return tagId;
    }

    public Tag setTagId(int tagId) {
        this.tagId = tagId;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Tag setDescription(String description) {
        this.description = description;
        return this;
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt("id", getTagId());
        bundle.putString("description",getDescription());
        return bundle;
    }

    public static class Builder {
        private Tag tag;
        private List<Tag> tags;

        public Builder() {
            tag = new Tag();
            tags = new ArrayList<>();
        }

        public Builder(Tag tag) {
            this.tag = tag;
        }

        public Builder(List<Tag> tags) {
            this.tags = tags;
        }

        public Tag build() {
            return tag;
        }

        public List<Tag> buildAll() {
            return tags;
        }

        public static Builder from(JSONObject jsonTag) {
            try {
                return new Builder(new Tag(
                        jsonTag.getInt("id"),
                        jsonTag.optString("description")
                ));
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }

        public static Builder from(JSONArray jsonTags) {
            List<Tag> tags = new ArrayList<>();
            int length = jsonTags.length();
            try {
                for(int i = 0; i < length; i++ )
                    tags.add(Tag.Builder.from(jsonTags.getJSONObject(i)).build());
            } catch(JSONException e) {
                e.printStackTrace();
            }
            return new Builder(tags);
        }


        public static Builder from(Bundle bundle) {
            return new Builder(new Tag(
                    bundle.getInt("id"),
                    bundle.getString("description")
            ));
        }
    }
}
