package com.moviles.sergioquiroz.tratohechov2.viewControllers.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.moviles.sergioquiroz.tratohechov2.R;
import com.moviles.sergioquiroz.tratohechov2.models.Customers;
import com.moviles.sergioquiroz.tratohechov2.viewControllers.activities.ProfileEditActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    ViewHolder holder;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_profile, container, false);
        Bundle bundle = getArguments();
        holder = new ViewHolder(view, getArguments());
        holder.getInfo(Customers.Builder.from(bundle).build());
        return view;
    }

    private class ViewHolder{
        private TextView textName;
        private TextView textDescription;
        private TextView textEmail;
        private TextView textAddress;
        private TextView textReference;
        private TextView textPhone;
        private TextView textTypeDoc;
        private TextView textNumberDoc;
        private RatingBar ratingBar;
        private FloatingActionButton fabEdit;


        public ViewHolder(View view, final Bundle bundle){
            textName = (TextView) view.findViewById(R.id.name_user);
            fabEdit = (FloatingActionButton) view.findViewById(R.id.fab_edit_user);
            textDescription = (TextView) view.findViewById(R.id.description_user);
            textEmail = (TextView) view.findViewById(R.id.email_user);
            textAddress = (TextView) view.findViewById(R.id.address_user);
            textReference = (TextView) view.findViewById(R.id.reference_user);
            textPhone = (TextView) view.findViewById(R.id.phone_user);
            textTypeDoc = (TextView) view.findViewById(R.id.typeDoc_user);
            textNumberDoc = (TextView) view.findViewById(R.id.numberDoc_user);
            ratingBar = (RatingBar) view.findViewById(R.id.ratingBar_customer);
            fabEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = getContext();
                    Intent intent = new Intent(context, ProfileEditActivity.class);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }
            });
        }

        private void getInfo(Customers customer) {
            String typeDoc;
            String name = customer.getNames() + customer.getLastNames();
            textName.setText(name);
            textDescription.setText(customer.getLogin());
            textEmail.setText(customer.getEmail());
            textAddress.setText(customer.getAddress());
            textReference.setText(customer.getReference());
            textPhone.setText(customer.getPhone());
            if (customer.getDocType().getDocTypeId() == 1) {
                typeDoc = "DNI";
                textTypeDoc.setText(typeDoc);
            }
            textNumberDoc.setText(customer.getDocType().getDescription());
            ratingBar.setRating((float)customer.getRate());
        }
    }
}
