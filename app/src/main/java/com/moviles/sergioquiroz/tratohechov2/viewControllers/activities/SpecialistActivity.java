package com.moviles.sergioquiroz.tratohechov2.viewControllers.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.moviles.sergioquiroz.tratohechov2.models.Favorite;
import com.moviles.sergioquiroz.tratohechov2.models.Problems;
import com.moviles.sergioquiroz.tratohechov2.R;
import com.moviles.sergioquiroz.tratohechov2.models.Specialists;
import com.moviles.sergioquiroz.tratohechov2.network.ServicesApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SpecialistActivity extends AppCompatActivity {
    ViewHolder holder;
    Specialists specialist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specialist);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        if (intent == null) return;
        specialist = Specialists.Builder.from(intent.getExtras()).build();
        holder = new ViewHolder(specialist);
        holder.getInfo(specialist);
    }

    public class ViewHolder{
        private SharedPreferences sharedPreferences;
        private TextView nameTextView;
        private TextView tagTextView;
        private TextView descriptionTextView;
        private TextView experienceTextView;
        private RatingBar ratingBar;
        List<Problems> problems;
        List<String> problemsTitle;
        ImageButton btnFavorite;
        Button btnSendProblem;
        Favorite favorite;
        ListView listView;
        AlertDialog dialog;
        AlertDialog.Builder builder;
        String name;
        private int key;
        private static final String TAG = "TratoHecho";

        public ViewHolder(final Specialists specialist){
            sharedPreferences = getSharedPreferences("TratoHecho", MODE_PRIVATE);
            if (sharedPreferences.contains("customerKey"))
                key = sharedPreferences.getInt("customerKey",-1);
            problems = new ArrayList<>();
            problemsTitle = new ArrayList<String>();
            nameTextView = (TextView) findViewById(R.id.name_specialist);
            tagTextView = (TextView) findViewById(R.id.tag_specialist);
            descriptionTextView = (TextView) findViewById(R.id.description_specialist);
            experienceTextView = (TextView) findViewById(R.id.experience_specialist);
            btnSendProblem = (Button) findViewById(R.id.button_send_problem_specialist) ;
            ratingBar = (RatingBar) findViewById(R.id.ratingBar_specialist);
            btnFavorite = (ImageButton) findViewById(R.id.favorite_specialist);
            updateData(key);
            for (int i = 0; i < problems.size(); i++){
                problemsTitle.add(problems.get(i).getTitle());
            }
            listView = new ListView(SpecialistActivity.this);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(SpecialistActivity.this,
                    R.layout.item_list_specialist,
                    R.id.text_item_specialist, problemsTitle);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(SpecialistActivity.this, "Problema Enviado",Toast.LENGTH_SHORT).show();
                    dialog.hide();
                }
            });
            btnSendProblem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDialogListView(v);
                }
            });
            if (specialist.isHidden()) {
                btnFavorite.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                btnFavorite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        btnFavorite.setImageResource(R.drawable.ic_favorite_black_24dp);
                        setFavorite(key, specialist);
                    }
                });
            }
            else{
                btnFavorite.setImageResource(R.drawable.ic_favorite_black_24dp);
                btnFavorite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        btnFavorite.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                        unSetFavorite(key, favorite);
                    }
                });
            }
        }

        public void getInfo(Specialists specialist){
            name = specialist.getNames() + specialist.getLastNames();
            nameTextView.setText(name);
            tagTextView.setText(specialist.getCompanyName());
            descriptionTextView.setText(specialist.getDescription());
            experienceTextView.setText(specialist.getWeb());
            ratingBar.setRating((float)specialist.getRate());
        }

        public void showDialogListView(View view){
            if (dialog == null) {
                builder = new AlertDialog.Builder(SpecialistActivity.this);
                builder.setCancelable(true);
                builder.setTitle("Problemas");
                builder.setPositiveButton("Cancelar", null);
                builder.setView(listView);
                dialog = builder.create();
                dialog.show();
            }
            else
                dialog.show();
        }

        public void updateData(int key){
            AndroidNetworking.get(ServicesApi.getCustomerProblems(String.valueOf(key)))
                    .setTag(TAG)
                    .setPriority(Priority.LOW)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                problems = Problems.Builder.from(
                                        response.optJSONArray("problems")).buildAll();
                                //adapter.setProblems(problems);
                            } catch (Exception e){
                                Log.d(TAG, String.format("Response Exception: %s", e.getMessage()));
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d(TAG, String.format("onError: %s", anError.getErrorDetail()));
                        }
                    });
        }

        public void setFavorite(int key, Specialists specialists){
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("customer", jsonObject.put("id", String.valueOf(key)));
                jsonObject.put("specialist", jsonObject.put("id", String.valueOf(specialists.getSpecialistId())));
                jsonObject.put("hidden", "false");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            AndroidNetworking.post(ServicesApi.getCustomerFavorites(String.valueOf(key)))
                    .addJSONObjectBody(jsonObject)
                    .setTag(TAG)
                    .setPriority(Priority.LOW)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                favorite = Favorite.Builder.from(response).build();
                                finish();
                                Toast.makeText(getApplicationContext(),
                                        "Favorito Agregado", Toast.LENGTH_SHORT).show();
                            } catch (Exception e){
                                Log.d(TAG, String.format("Response Exception: %s", e.getMessage()));
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d(TAG, String.format("onError: %s", anError.getErrorDetail()));
                        }
                    });
        }

        public void unSetFavorite(int key, Favorite favorite){
            AndroidNetworking.delete(ServicesApi.deleteCustomerFavorite(String.valueOf(key),
                    String.valueOf(favorite.getFavoriteId())))
                    .setTag(TAG)
                    .setPriority(Priority.LOW)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                finish();
                                Toast.makeText(getApplicationContext(),
                                        "Favorito Eliminado", Toast.LENGTH_SHORT).show();
                            } catch (Exception e){
                                Log.d(TAG, String.format("Response Exception: %s", e.getMessage()));
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d(TAG, String.format("onError: %s", anError.getErrorDetail()));
                        }
                    });
        }
    }
}
