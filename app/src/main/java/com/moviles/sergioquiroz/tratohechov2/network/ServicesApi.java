package com.moviles.sergioquiroz.tratohechov2.network;

public class ServicesApi {
    private static String BASE_URL = "http://movilesupc.somee.com/api/";

    public static String getTagsUrl() {
        return BASE_URL + "tags";
    }

    public static String getSpecialistsUrl() {
        return BASE_URL + "specialists";
    }

    public static String getSpecialistsById(String id) { return BASE_URL + "specialists/" + id; }

    public static String getClientsUrl() { return BASE_URL + "customers"; }

    public static String getLoginCustomer() {return BASE_URL + "customers/authentications";}

    public static String getDocumentTypesUrl() {
        return BASE_URL + "documenttypes";
    }

    public static String getCustomerById(String id) { return BASE_URL + "customers/" + id; }

    public static String getCustomerProblems(String id) { return BASE_URL + "customers/" + id + "/problems"; }

    public static String getCustomerFavorites(String id) { return BASE_URL + "customers/" + id + "/favorites";}

    public static String deleteCustomerFavorite(String idCustomer, String idFavorite){
        return BASE_URL + "customers/" + idCustomer + "/favorites/" + idFavorite;
    }

    public static String getCustomerProblemsQuotations(String idCustomer, String idProblems){
        return BASE_URL + "customers/" + idCustomer + "/problems/" + idProblems + "/quotations";
    }
}
