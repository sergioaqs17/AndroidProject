package com.moviles.sergioquiroz.tratohechov2.viewControllers.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.moviles.sergioquiroz.tratohechov2.R;
import com.moviles.sergioquiroz.tratohechov2.models.Customers;
import com.moviles.sergioquiroz.tratohechov2.network.ServicesApi;

import org.json.JSONObject;

import java.util.List;

public class LoginActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    ViewHolder holder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sharedPreferences = getSharedPreferences("TratoHecho", Context.MODE_PRIVATE);
        holder = new ViewHolder();
    }

    private class ViewHolder {
        TextInputLayout textInputUsername;
        TextInputLayout textInputPassword;
        String name;
        private static final String TAG = "Login";

        public ViewHolder(){
            textInputUsername = (TextInputLayout) findViewById(R.id.text_input_username);
            textInputPassword = (TextInputLayout) findViewById(R.id.text_input_password);

            findViewById(R.id.button_login).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    valid(textInputUsername.getEditText().getText().toString(),
                            textInputPassword.getEditText().getText().toString(), v);
                }
            });

            findViewById(R.id.button_singUp).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(v.getContext(), SingUpActivity.class));
                }
            });
        }

        public void valid(String username, String password, final View view){
            AndroidNetworking.post(ServicesApi.getLoginCustomer())
                    .addBodyParameter("login", username)
                    .addBodyParameter("password", password)
                    .setTag(TAG)
                    .setPriority(Priority.LOW)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try{
                                Customers customer = Customers.Builder.from(response).build();
                                navigateToMain(customer, view);
                            } catch (Exception e) {
                                Log.d(TAG, String.format("Response Exception: %s", e.getMessage()));
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toast.makeText(getApplicationContext(),
                                    "Username or Password invalid",
                                    Toast.LENGTH_SHORT).show();
                            Log.d(TAG, String.format("onError: %s", anError.getErrorDetail()));
                        }
                    });
        }

        private void navigateToMain(Customers customer, View v) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt("customerKey", customer.getCustomerId());
            editor.commit();
            name = customer.getNames();
            Toast.makeText(getApplicationContext(), "Welcome "
                    + name, Toast.LENGTH_SHORT).show();
            Intent launcher=new Intent(LoginActivity.this,MainCustomerActivity.class);
            //Log.d("modelo de usuario", String.valueOf(customer));
            launcher.putExtras(customer.toBundle());
            startActivity(launcher);
        }
    }
}
