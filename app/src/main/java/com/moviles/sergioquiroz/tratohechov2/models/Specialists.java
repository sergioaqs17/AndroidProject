package com.moviles.sergioquiroz.tratohechov2.models;

import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Specialists {
    private int specialistId;
    private String login;
    private String password;
    private String lastNames;
    private String names;
    private String companyName;
    private String description;
    private DocType docType;
    private String phone;
    private String facebook;
    private String web;
    private String address;
    private String reference;
    private double latitude;
    private double longitude;
    private boolean accredited;
    private boolean membership;
    private double rate;
    private boolean state;
    private boolean hidden;
    private String email;

    public Specialists() {
    }

    public Specialists(int specialistId, String login, String password, String lastNames,
                       String names, String companyName, String description, DocType docType,
                       String phone, String facebook, String web, String address,
                       String reference, double latitude, double longitude, boolean accredited,
                       boolean membership, double rate, boolean state, boolean hidden, String email) {
        this.specialistId = specialistId;
        this.login = login;
        this.password = password;
        this.lastNames = lastNames;
        this.names = names;
        this.companyName = companyName;
        this.description = description;
        this.docType = docType;
        this.phone = phone;
        this.facebook = facebook;
        this.web = web;
        this.address = address;
        this.reference = reference;
        this.latitude = latitude;
        this.longitude = longitude;
        this.accredited = accredited;
        this.membership = membership;
        this.rate = rate;
        this.state = state;
        this.hidden = hidden;
        this.email = email;
    }

    public Specialists(int specialistId) {
        this.specialistId = specialistId;
    }

    public int getSpecialistId() {
        return specialistId;
    }

    public Specialists setSpecialistId(int specialistId) {
        this.specialistId = specialistId;
        return this;
    }

    public String getLogin() {
        return login;
    }

    public Specialists setLogin(String login) {
        this.login = login;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Specialists setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getLastNames() {
        return lastNames;
    }

    public Specialists setLastNames(String lastNames) {
        this.lastNames = lastNames;
        return this;
    }

    public String getNames() {
        return names;
    }

    public Specialists setNames(String names) {
        this.names = names;
        return this;
    }

    public String getCompanyName() {
        return companyName;
    }

    public Specialists setCompanyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Specialists setDescription(String description) {
        this.description = description;
        return this;
    }

    public DocType getDocType() {
        return docType;
    }

    public Specialists setDocType(DocType docType) {
        this.docType = docType;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public Specialists setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getFacebook() {
        return facebook;
    }

    public Specialists setFacebook(String facebook) {
        this.facebook = facebook;
        return this;
    }

    public String getWeb() {
        return web;
    }

    public Specialists setWeb(String web) {
        this.web = web;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Specialists setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getReference() {
        return reference;
    }

    public Specialists setReference(String reference) {
        this.reference = reference;
        return this;
    }

    public double getLatitude() {
        return latitude;
    }

    public Specialists setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public double getLongitude() {
        return longitude;
    }

    public Specialists setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public boolean isAccredited() {
        return accredited;
    }

    public Specialists setAccredited(boolean accredited) {
        this.accredited = accredited;
        return this;
    }

    public boolean isMembership() {
        return membership;
    }

    public Specialists setMembership(boolean membership) {
        this.membership = membership;
        return this;
    }

    public double getRate() {
        return rate;
    }

    public Specialists setRate(double rate) {
        this.rate = rate;
        return this;
    }

    public boolean isState() {
        return state;
    }

    public Specialists setState(boolean state) {
        this.state = state;
        return this;
    }

    public boolean isHidden() {
        return hidden;
    }

    public Specialists setHidden(boolean hidden) {
        this.hidden = hidden;
        return this;
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt("id", getSpecialistId());
        bundle.putString("login", getLogin());
        bundle.putString("password", getPassword());
        bundle.putString("name", getNames());
        bundle.putString("lastName", getLastNames());
        bundle.putString("companyName", getCompanyName());
        bundle.putString("serviceDescription", getDescription());
        bundle.putBundle("document", getDocType().toBundle());
        bundle.putString("phoneNumber", getPhone());
        bundle.putString("facebook", getFacebook());
        bundle.putString("webSite", getWeb());
        bundle.putString("address", getAddress());
        bundle.putString("reference", getReference());
        bundle.putDouble("latitude", getLatitude());
        bundle.putDouble("longitude", getLongitude());
        bundle.putBoolean("acredited", isAccredited());
        bundle.putBoolean("memberShip", isMembership());
        bundle.putDouble("rate", getRate());
        bundle.putBoolean("state", isState());
        bundle.putBoolean("online", isHidden());
        bundle.putString("email", getEmail());
        return bundle;
    }

    public String getEmail() {
        return email;
    }

    public Specialists setEmail(String email) {
        this.email = email;
        return this;
    }

    public static class Builder {
        Specialists specialist;
        List<Specialists> specialists;

        public Builder() {
        }

        public Builder(Specialists specialist) {
            this.specialist = specialist;
        }

        public Builder(List<Specialists> specialists) {
            this.specialists = specialists;
        }

        public Specialists build() {
            return specialist;
        }

        public List<Specialists> buildAll() {
            return specialists;
        }

        public static Builder from(JSONObject jsonSpecialist) {
            try {
                return new Builder(new Specialists(
                        jsonSpecialist.getInt("id"),
                        jsonSpecialist.getString("login"),
                        jsonSpecialist.getString("password"),
                        jsonSpecialist.getString("name"),
                        jsonSpecialist.getString("lastName"),
                        jsonSpecialist.getString("companyName"),
                        jsonSpecialist.getString("serviceDescription"),
                        DocType.Builder.from(jsonSpecialist.getJSONObject("document")).build(),
                        jsonSpecialist.getString("phoneNumber"),
                        jsonSpecialist.getString("facebook"),
                        jsonSpecialist.getString("webSite"),
                        jsonSpecialist.getString("address"),
                        jsonSpecialist.getString("reference"),
                        jsonSpecialist.getDouble("latitude"),
                        jsonSpecialist.getDouble("longitude"),
                        jsonSpecialist.getBoolean("acredited"),
                        jsonSpecialist.getBoolean("memberShip"),
                        jsonSpecialist.getDouble("rate"),
                        jsonSpecialist.getBoolean("state"),
                        jsonSpecialist.getBoolean("online"),
                        jsonSpecialist.getString("email")
                ));
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }

        public static Builder from(JSONArray jsonSpecialists) {
            int length = jsonSpecialists.length();
            List<Specialists> specialists = new ArrayList<>();
            for (int i = 0; i < length; i++) {
                try {
                    specialists.add(Builder.from(jsonSpecialists.getJSONObject(i)).build());
                } catch (JSONException | NullPointerException e) {
                    e.printStackTrace();
                }
            }
            return new Builder(specialists);
        }

        public static Builder from(Bundle bundle) {
            return new Builder(new Specialists(
                    bundle.getInt("id"),
                    bundle.getString("login"),
                    bundle.getString("password"),
                    bundle.getString("name"),
                    bundle.getString("lastName"),
                    bundle.getString("companyName"),
                    bundle.getString("serviceDescription"),
                    DocType.Builder.from(bundle.getBundle("document")).build(),
                    bundle.getString("phoneNumber"),
                    bundle.getString("facebook"),
                    bundle.getString("webSite"),
                    bundle.getString("address"),
                    bundle.getString("reference"),
                    bundle.getDouble("latitude"),
                    bundle.getDouble("longitude"),
                    bundle.getBoolean("acredited"),
                    bundle.getBoolean("memberShip"),
                    bundle.getDouble("rate"),
                    bundle.getBoolean("state"),
                    bundle.getBoolean("online"),
                    bundle.getString("email")
            ));
        }
    }
}
