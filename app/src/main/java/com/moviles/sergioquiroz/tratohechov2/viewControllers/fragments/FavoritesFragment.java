package com.moviles.sergioquiroz.tratohechov2.viewControllers.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.moviles.sergioquiroz.tratohechov2.models.Customers;
import com.moviles.sergioquiroz.tratohechov2.models.Favorite;
import com.moviles.sergioquiroz.tratohechov2.R;
import com.moviles.sergioquiroz.tratohechov2.models.Specialists;
import com.moviles.sergioquiroz.tratohechov2.network.ServicesApi;
import com.moviles.sergioquiroz.tratohechov2.viewControllers.adapters.SpecialistsAdapter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritesFragment extends Fragment {
    RecyclerView specialistRecyclerView;
    SpecialistsAdapter adapter;
    LinearLayoutManager layoutManager;
    List<Favorite> favorites;
    Customers customers;
    List<Specialists> specialists;
    private static final String TAG = "TratoHecho";

    public FavoritesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);
        specialistRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_favorites);
        specialists = new ArrayList<>();
        adapter = new SpecialistsAdapter(specialists);
        layoutManager = new LinearLayoutManager(view.getContext());
        specialistRecyclerView.setAdapter(adapter);
        specialistRecyclerView.setLayoutManager(layoutManager);
        Bundle bundle = getArguments();
        customers = Customers.Builder.from(bundle).build();
        updateData(customers);
        return view;
    }

    private void updateData(Customers customer){
        AndroidNetworking.get(ServicesApi.getCustomerFavorites(String.valueOf(customer.getCustomerId())))
                .setTag(TAG)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            favorites = Favorite.Builder.from(
                                    response.optJSONArray("favorites")).buildAll();
                            adapter.setSpecialists(getSpecialistsFromFavorites(favorites));
                            adapter.notifyDataSetChanged();
                        } catch (Exception e){
                            Log.d(TAG, String.format("Response Exception: %s", e.getMessage()));
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, String.format("onError: %s", anError.getErrorDetail()));
                    }
                });
    }

    private List<Specialists> getSpecialistsFromFavorites(List<Favorite> favorites){
        int length = favorites.size();
        for (int i = 0; i < length; i++){
            if (!favorites.get(i).isHidden())
                specialists.add(favorites.get(i).getSpecialistId());
        }
        return specialists;
    }

}
