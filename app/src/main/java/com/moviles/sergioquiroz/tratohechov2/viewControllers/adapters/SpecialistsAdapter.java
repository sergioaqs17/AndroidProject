package com.moviles.sergioquiroz.tratohechov2.viewControllers.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.moviles.sergioquiroz.tratohechov2.R;
import com.moviles.sergioquiroz.tratohechov2.models.Specialists;
import com.moviles.sergioquiroz.tratohechov2.viewControllers.activities.SpecialistActivity;

import java.util.List;

public class SpecialistsAdapter extends RecyclerView.Adapter<SpecialistsAdapter.ViewHolder> {
    private List<Specialists> specialists;
    private Specialists specialist;

    public SpecialistsAdapter(List<Specialists> specialists) {
        this.specialists = specialists;
    }

    public SpecialistsAdapter() {
    }

    @NonNull
    @Override
    public SpecialistsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
            .inflate(R.layout.item_specialist,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull SpecialistsAdapter.ViewHolder holder, int position) {
        holder.updateViews(specialists.get(position));
    }

    @Override
    public int getItemCount() {
        return specialists.size();
    }

    public Specialists getSpecialist() {
        return specialist;
    }

    public SpecialistsAdapter setSpecialist(Specialists specialist) {
        this.specialist = specialist;
        return this;
    }

    public List<Specialists> getSpecialists() {
        return specialists;
    }

    public SpecialistsAdapter setSpecialists(List<Specialists> specialists) {
        this.specialists = specialists;
        return this;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView;
        private TextView tagTextView;
        private RatingBar ratingBar;
        private ConstraintLayout itemLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.title_specialist);
            tagTextView = (TextView) itemView.findViewById(R.id.tag_specialist);
            itemLayout = (ConstraintLayout) itemView.findViewById(R.id.layout_item);
            ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar_specialist);
        }

        public void updateViews(final Specialists fromSpecialist){
            nameTextView.setText(fromSpecialist.getNames());
            tagTextView.setText(fromSpecialist.getDescription());
            ratingBar.setRating((float)fromSpecialist.getRate());
            itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    context.startActivity(new Intent(context, SpecialistActivity.class)
                            .putExtras(fromSpecialist.toBundle()));
                }
            });
        }
    }


}
