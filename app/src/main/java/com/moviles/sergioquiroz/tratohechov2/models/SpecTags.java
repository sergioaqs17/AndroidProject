package com.moviles.sergioquiroz.tratohechov2.models;

import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SpecTags {
    private int specTagId;
    private Tag tagId;
    private Specialists specialistId;
    private boolean hidden;

    public SpecTags() {
    }

    public SpecTags(int specTagId, Tag tagId, Specialists specialistId, boolean hidden) {
        this.specTagId = specTagId;
        this.tagId = tagId;
        this.specialistId = specialistId;
        this.hidden = hidden;
    }

    public int getSpecTagId() {
        return specTagId;
    }

    public SpecTags setSpecTagId(int specTagId) {
        this.specTagId = specTagId;
        return this;
    }

    public Tag getTagId() {
        return tagId;
    }

    public SpecTags setTagId(Tag tagId) {
        this.tagId = tagId;
        return this;
    }

    public Specialists getSpecialistId() {
        return specialistId;
    }

    public SpecTags setSpecialistId(Specialists specialistId) {
        this.specialistId = specialistId;
        return this;
    }

    public boolean isHidden() {
        return hidden;
    }

    public SpecTags setHidden(boolean hidden) {
        this.hidden = hidden;
        return this;
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt("id", getSpecTagId());
        bundle.putBundle("tag", getTagId().toBundle());
        bundle.putBundle("specialist", getSpecialistId().toBundle());
        bundle.putBoolean("hidden", isHidden());
        return bundle;
    }

    public static class Builder {
        SpecTags specTags;
        List<SpecTags> specTagsList;

        public Builder() {
            specTags = new SpecTags();
            specTagsList = new ArrayList<>();
        }

        public Builder(SpecTags specTags) {
            this.specTags = specTags;
        }

        public Builder(List<SpecTags> specTagsList) {
            this.specTagsList = specTagsList;
        }

        public SpecTags build() {
            return specTags;
        }

        public List<SpecTags> buildAll() {
            return specTagsList;
        }

        public static Builder from(JSONObject jsonSpecTags) {
            try {
                return new Builder(new SpecTags(
                        jsonSpecTags.getInt("id"),
                        Tag.Builder.from(jsonSpecTags.getJSONObject("tag")).build(),
                        Specialists.Builder.from(jsonSpecTags.getJSONObject("specialist")).build(),
                        jsonSpecTags.getBoolean("hidden")
                ));
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }

        public static Builder from(JSONArray jsonSpecTagsList) {
            int length = jsonSpecTagsList.length();
            List<SpecTags> specTagsList = new ArrayList<>();
            for (int i = 0; i < length; i++) {
                try {
                    specTagsList.add(Builder.from(jsonSpecTagsList.getJSONObject(i)).build());
                } catch (JSONException | NullPointerException e) {
                    e.printStackTrace();
                }
            }
            return new Builder(specTagsList);
        }

        public static Builder from(Bundle bundle) {
            return new Builder(new SpecTags(
                    bundle.getInt("id"),
                    Tag.Builder.from(bundle.getBundle("tag")).build(),
                    Specialists.Builder.from(bundle.getBundle("specialist")).build(),
                    bundle.getBoolean("hidden")
            ));
        }
    }

}
