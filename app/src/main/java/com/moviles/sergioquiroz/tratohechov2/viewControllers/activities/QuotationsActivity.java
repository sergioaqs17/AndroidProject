package com.moviles.sergioquiroz.tratohechov2.viewControllers.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.moviles.sergioquiroz.tratohechov2.R;
import com.moviles.sergioquiroz.tratohechov2.models.Problems;
import com.moviles.sergioquiroz.tratohechov2.models.Quotations;
import com.moviles.sergioquiroz.tratohechov2.network.ServicesApi;
import com.moviles.sergioquiroz.tratohechov2.viewControllers.adapters.QuotationsAdapter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class QuotationsActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    RecyclerView quotationsRecyclerView;
    QuotationsAdapter adapter;
    LinearLayoutManager layoutManager;
    Problems problems;
    List<Quotations> quotations;
    private int key;
    private static final String TAG = "TratoHecho";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotations);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sharedPreferences = getSharedPreferences("TratoHecho", MODE_PRIVATE);
        if (sharedPreferences.contains("customerKey"))
            key = sharedPreferences.getInt("customerKey",-1);
        quotationsRecyclerView = (RecyclerView) findViewById(R.id.recycler_quotations);
        quotations = new ArrayList<>();
        adapter = new QuotationsAdapter(quotations);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        quotationsRecyclerView.setAdapter(adapter);
        quotationsRecyclerView.setLayoutManager(layoutManager);
        Intent intent = getIntent();
        if (intent == null) return;
        problems =  Problems.Builder.from(Objects.requireNonNull(intent.getExtras())).build();
        updateData(problems, key);
    }

    private void updateData(Problems problems, int key) {
        AndroidNetworking.get(ServicesApi.getCustomerProblemsQuotations(String.valueOf(key),
                String.valueOf(problems.getProblemsId())))
                .setTag(TAG)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            quotations = Quotations.Builder.from(
                                    response.optJSONArray("quotations")).buildAll();
                            adapter.setQuotations(quotations);
                            adapter.notifyDataSetChanged();
                        } catch (Exception e){
                            Log.d(TAG, String.format("Response Exception: %s", e.getMessage()));
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, String.format("onError: %s", anError.getErrorDetail()));
                    }
                });
    }

}
